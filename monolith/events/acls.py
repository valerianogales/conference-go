from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY
import json
import requests


# function that makes a request to use the Pexels API
def get_photo(city, state):
    # Use the Pexels API
    # Doing the request
    url = "https://api.pexels.com/v1/search"
    headers = {"Authorization": PEXELS_API_KEY}
    payload = {
        "query": f"{city} {state}",
        "per_page": 1,
    }

    response = requests.get(url, params=payload, headers=headers)

    # Pull oout the data from the request
    picture = json.loads(response.content)

    try:
        return picture["photos"][0]["url"]
    except (KeyError, IndexError):
        return None


# function to make a request to the Open Weather API


def get_weather_data(city, state):
    # Use the Open Weather API
    weather_response = {}

    url = "http://api.openweathermap.org/geo/1.0/direct?"
    payload = {
        "q": f"{city},{state}, US",
        "appid": {OPEN_WEATHER_API_KEY},
        "limit": 3,
    }

    lat_lon_response = requests.get(url, params=payload)
    lat_lon = json.loads(lat_lon_response.content)

    lat = lat_lon[0]["lat"]
    lon = lat_lon[0]["lon"]

    url = "https://api.openweathermap.org/data/2.5/weather?"
    payload = {
        "lat": lat,
        "lon": lon,
        "appid": {OPEN_WEATHER_API_KEY},
    }

    response = requests.get(url, params=payload)

    weather = json.loads(response.content)

    weather_response["temp"] = weather["main"]["temp"]
    weather_response["weather_description"] = weather["weather"][0][
        "description"
    ]

    try:
        return weather_response
    except (KeyError, IndexError):
        return None

    # Create the URL for the geocoding API with the city and state

    # Make the request
    # Parse the JSON response
    # Get the latitude and longitude from the response

    # Create the URL for the current weather API with the latitude
    #   and longitude
    # Make the request
    # Parse the JSON response
    # Get the main temperature and the weather's description and put
    #   them in a dictionary
    # Return the dictionary
